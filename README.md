# OWeather Application

This is application project to apply for Android developer
```bash
git clone https://gitlab.com/hoanghq90/owapp.git
```

## **1. Application Architecture:**

- Application Architecture: **MVVM**
- Dependency Injection Framework: **Koin**
- Networking: **Retrofit 2**
- Multithreading: **Coroutine**
- Encrypt: **EncryptedSharedPreferences**
- Testing: JUnit4, Mockito.

![Application Architecture](DataFlowDiagram.png)

## **2. Folder structure:**
- _/configuration_: Include class & interface persistent configuration
- _/data_: Include repositories persistent data.
- _/di_: Include the dependence injection.
- _/encryption_: Include class & interface encrypt data.
- _/model_: Include models
- _/networking_: Include networking's class & interface
- _/ui_: Include View(Activity, Fragment) Adapter & ViewHolder
- _/viewmodel_: Include ViewModels


## **3. Installation: **
#### Build variants
Use the Android Studio *Build Variants* button to choose between **production** and **develop** flavors combined with debug and release build types

##### Install develop:
1. Run ```./gradlew installDevelopDebug ``` (or import to ``Android Studio`` & run )
2. Start ```adb shell am start -n "com.abastudio.owapp.dev/com.abastudio.owapp.MainActivity" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER```
##### Install production:
1. Go to release production foler ```cd release/production/```
2. Build ```./build.sh``` the apk file will generate at ```projectRoot/app/build/outputs/apk/production/release/app-production-release.apk```
3. Install ```adb install ../../app/build/outputs/apk/production/release/app-production-release.apk```
4. Start ```adb shell am start -n "com.abastudio.owapp/com.abastudio.owapp.MainActivity" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER```

##### Keystores & API Information:
Production's eystore & API information store at projectRoot/release/production
- _/release_/production_/api.properties_: Encrypted API information
- _/release_/production_/keystore_.properties_: Keystore information
- _/release_/production_/keystore.jks_: Keystore file

** For CD for production, push 3 file about to server & update build.sh to build.**

## **4. Check list:**
- [x] The application is a simple Android application which is written by Java/Kotlin.
- [x] The application is able to retrieve the weather information from OpenWeatherMaps API.
- [x] The application is able to allow user to input the searching term.
- [x] The application is able to proceed searching with a condition of the search term length
must be from 3 characters or above.
- [x] The application is able to render the searched results as a list of weather items.
- [x] The application is able to support caching mechanism so as to prevent the app from
generating a bunch of API requests.
- [x] The application is able to manage caching mechanism & lifecycle.
- [x] The application is able to handle failures.
- [x] The application is able to support the disability to scale large text for who can't see the
text clearly.
- [x] The application is able to support the disability to read out the text using VoiceOver
controls.