#!/usr/bin/env bash
cp ./api.properties ../../
cp ./keystore.jks ../../
cp ./keystore.properties ../../
cd ../../
./gradlew assembleProductionRelease