package com.abastudio.owapp.model

import java.util.*

data class Weather(
    var dateTime: Date? = null,
    var averageTemperature: Double? = null,
    var pressure: Int? = null,
    var humidity: Int? = null,
    var description: String? = null
)