package com.abastudio.owapp.model

enum class TemperatureUnit(val value: String) {
    STANDARD("standard"),
    METRIC("metric"),
    IMPERIAL("imperial"),
}