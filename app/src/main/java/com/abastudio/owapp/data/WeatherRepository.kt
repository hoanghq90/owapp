package com.abastudio.owapp.data

import android.util.Log
import com.abastudio.owapp.model.Weather
import com.abastudio.owapp.networking.IService
import com.abastudio.owapp.networking.caching.CachingProvider
import com.abastudio.owapp.networking.caching.ICaching
import com.abastudio.owapp.ui.search.SearchQueryParams
import org.koin.core.KoinComponent
import org.koin.core.inject

class WeatherRepository : IWeatherRepository, KoinComponent {
    private val service: IService by inject()
    private val cachingData: ICaching = CachingProvider().provide()
    override suspend fun getWeather(params: SearchQueryParams): List<Weather> {
        val cache: List<Weather>? = cachingData.get(params)
        if (cache != null) {
            Log.d("WeatherRepository", "getWeather: from caching")
            return cache
        }
        val weathers = service.getWeather(params.query, params.cnt, params.units)
        Log.d("WeatherRepository", "getWeather: from service")
        cachingData.push(params, weathers)
        return weathers

    }

}