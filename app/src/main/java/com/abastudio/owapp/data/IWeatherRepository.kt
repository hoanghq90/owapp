package com.abastudio.owapp.data

import com.abastudio.owapp.model.Weather
import com.abastudio.owapp.ui.search.SearchQueryParams
import retrofit2.HttpException

interface IWeatherRepository {
    @Throws(HttpException::class, Exception::class)
    suspend fun getWeather(params: SearchQueryParams): List<Weather>
}