package com.abastudio.owapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.abastudio.owapp.configuration.IConfiguration
import com.abastudio.owapp.data.IWeatherRepository
import com.abastudio.owapp.ui.search.SearchQueryParams
import com.abastudio.owapp.ui.search.SearchUIModel
import com.abastudio.owapp.ui.search.validation.IValidation
import com.abastudio.owapp.ui.search.validation.SearchQueryValidation
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.HttpException
import java.net.UnknownHostException

class WeatherViewModel : ViewModel(), KoinComponent {
    private val repository: IWeatherRepository by inject()
    private val configuration: IConfiguration by inject()
    private val queryValidation: IValidation<String> by lazy {
        SearchQueryValidation()
    }

    private val searchLiveData: MutableLiveData<List<SearchUIModel>> by lazy {
        MutableLiveData<List<SearchUIModel>>(listOf(SearchUIModel.FirstLookUIModel))
    }

    fun getSearchWeatherLiveData(): LiveData<List<SearchUIModel>> = searchLiveData

    fun fetchForecast(query: String) {
        GlobalScope.launch {
            processForecastRequest(query)
        }
    }

    suspend fun processForecastRequest(location: String) {
        val queryAfterTrim = location.trim()
        if (!queryValidation.isValid(queryAfterTrim)) {
            searchLiveData.postValue(listOf(SearchUIModel.ErrorUIMode("The query must least 3 character")))
            return
        }
        val params: SearchQueryParams = SearchQueryParams.Builder()
            .cnt(7)
            .units(configuration.getUnit().value)//get unit from configuration
            .query(queryAfterTrim)
            .build()
        val uiModel = try {
            searchLiveData.postValue(listOf(SearchUIModel.LoadingUIModel))
            repository.getWeather(params).map { weather ->
                SearchUIModel.WeatherUIMode(weather)
            }
        } catch (e: HttpException) {
            listOf(SearchUIModel.Empty)
        } catch (e: UnknownHostException) {
            listOf(SearchUIModel.ErrorUIMode("No network connection"))
        } catch (e: Exception) {
            listOf(SearchUIModel.ErrorUIMode("An unexpected error occurred"))
        }
        searchLiveData.postValue(uiModel)
    }

}