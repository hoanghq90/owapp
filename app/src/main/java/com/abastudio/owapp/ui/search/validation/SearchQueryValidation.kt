package com.abastudio.owapp.ui.search.validation

class SearchQueryValidation : IValidation<String> {
    override fun isValid(validatingValue: String) = validatingValue.trim().length >= 3
}