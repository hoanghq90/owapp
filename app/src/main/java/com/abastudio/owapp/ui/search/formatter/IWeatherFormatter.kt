package com.abastudio.owapp.ui.search.formatter

import java.util.*

interface IWeatherFormatter {
    fun formatDatetime(dateTime: Date?): String
    fun formatHumidity(humidity: Int?): String
    fun formatPressure(pressure: Int?): String
    fun formatTemperature(averageTemperature: Double?): String
}