package com.abastudio.owapp.ui.setting

import android.os.Bundle
import android.view.*
import android.widget.SeekBar
import androidx.navigation.fragment.findNavController
import com.abastudio.owapp.Constants
import com.abastudio.owapp.R
import com.abastudio.owapp.configuration.IConfiguration
import com.abastudio.owapp.databinding.SettingFragmentBinding
import com.abastudio.owapp.ui.BaseFragment
import org.koin.android.ext.android.inject

class SettingFragment : BaseFragment() {
    private lateinit var viewBinding: SettingFragmentBinding
    private val config: IConfiguration by inject()
    private val calculator = TextSizeScaleCalculator()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewBinding = SettingFragmentBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.sbTextSize.progress =
            calculator.scaleToProcess(
                config.getTextScale(),
                viewBinding.sbTextSize.max,
                Constants.MAXIMUM_SCALE_SIZE
            )
        viewBinding.sbTextSize.setOnSeekBarChangeListener(onTextSizeChange)
        config.saveState()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_setting, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onStop() {
        super.onStop()
        config.restore()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.done) {
            if (config.hasChange())
                activity?.recreate()
            findNavController().popBackStack()
        }
        return super.onOptionsItemSelected(item)
    }

    private val onTextSizeChange = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                val scale = calculator.processToScale(
                    progress,
                    viewBinding.sbTextSize.max,
                    Constants.MAXIMUM_SCALE_SIZE
                )
                config.setTextScale(scale)
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {}

        override fun onStopTrackingTouch(seekBar: SeekBar?) {}

    }

}