package com.abastudio.owapp.ui.search.holder

import android.view.View
import com.abastudio.owapp.R
import com.abastudio.owapp.databinding.ItemWeatherBinding
import com.abastudio.owapp.ui.search.SearchUIModel
import com.abastudio.owapp.ui.search.formatter.IWeatherFormatter
import com.abastudio.owapp.ui.search.formatter.WeatherFormatter

class WeatherHolder(itemView: View) : ItemHolder(itemView) {
    private val viewBinding: ItemWeatherBinding = ItemWeatherBinding.bind(itemView)
    private val formatter: IWeatherFormatter = WeatherFormatter()
    fun bind(weatherUIMode: SearchUIModel.WeatherUIMode) {
        val weather = weatherUIMode.weatherInDays
        viewBinding.run {
            viewBinding.txtDate.text = String.format(getString(R.string.item_weather_date), formatter.formatDatetime(weather.dateTime))
            viewBinding.txtHumidity.text = String.format(getString(R.string.item_weather_humidity), formatter.formatHumidity(weather.humidity))
            viewBinding.txtPressure.text = String.format(getString(R.string.item_weather_pressure), formatter.formatPressure(weather.pressure))
            viewBinding.txtTemperature.text = String.format(getString(R.string.item_weather_temperature), formatter.formatTemperature(weather.averageTemperature))
            viewBinding.txtDescription.text = String.format(getString(R.string.item_weather_description), weather.description)
        }
    }
}