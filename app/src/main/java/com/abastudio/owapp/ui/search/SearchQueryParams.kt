package com.abastudio.owapp.ui.search

class SearchQueryParams {
    var query: String = ""
    var cnt: Int = 7
    var units: String = "metric"

    class Builder() {
        private val params: SearchQueryParams by lazy { SearchQueryParams() }
        fun query(query: String): Builder {
            params.query = query
            return this
        }

        fun cnt(cnt: Int): Builder {
            params.cnt = cnt
            return this

        }

        fun units(units: String): Builder {
            params.units = units
            return this

        }

        fun build(): SearchQueryParams = params
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SearchQueryParams

        if (query != other.query) return false
        if (cnt != other.cnt) return false
        if (units != other.units) return false

        return true
    }

    override fun hashCode(): Int {
        var result = query.hashCode()
        result = 31 * result + cnt
        result = 31 * result + units.hashCode()
        return result
    }

}