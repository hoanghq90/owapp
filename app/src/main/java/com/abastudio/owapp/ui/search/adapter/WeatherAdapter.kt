package com.abastudio.owapp.ui.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.abastudio.owapp.R
import com.abastudio.owapp.ui.search.SearchUIModel
import com.abastudio.owapp.ui.search.holder.*

class WeatherAdapter : ListAdapter<SearchUIModel, ItemHolder>(DiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.item_weather_loading -> LoadingHolder(itemView)
            R.layout.item_weather_error -> ErrorHolder(itemView)
            R.layout.item_weather_first_look -> FirstLookHolder(itemView)
            R.layout.item_weather -> WeatherHolder(itemView)
            else -> EmptyHolder(itemView)
        }
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val item = getItem(position)
        when (holder) {
            is ErrorHolder -> holder.bind(item as SearchUIModel.ErrorUIMode)
            is LoadingHolder -> holder.bind()
            is FirstLookHolder -> holder.bind()
            is WeatherHolder -> holder.bind(item as SearchUIModel.WeatherUIMode)
            is EmptyHolder -> holder.bind()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (currentList[position]) {
            is SearchUIModel.Empty -> R.layout.item_weather_empty
            is SearchUIModel.ErrorUIMode -> R.layout.item_weather_error
            is SearchUIModel.LoadingUIModel -> R.layout.item_weather_loading
            is SearchUIModel.FirstLookUIModel -> R.layout.item_weather_first_look
            is SearchUIModel.WeatherUIMode -> R.layout.item_weather
            else -> R.layout.item_weather_empty
        }
    }


    companion object DiffCallback : DiffUtil.ItemCallback<SearchUIModel>() {

        override fun areItemsTheSame(oldItem: SearchUIModel, newItem: SearchUIModel): Boolean {
            val isSameEmpty = oldItem is SearchUIModel.Empty
                    && newItem is SearchUIModel.Empty
            val isSameFirstLook = oldItem is SearchUIModel.FirstLookUIModel
                    && newItem is SearchUIModel.FirstLookUIModel
            val isSameError = oldItem is SearchUIModel.ErrorUIMode
                    && newItem is SearchUIModel.ErrorUIMode
            val isSameWeather = oldItem is SearchUIModel.WeatherUIMode
                    && newItem is SearchUIModel.WeatherUIMode
            val isSameLoading = oldItem is SearchUIModel.LoadingUIModel
                    && newItem is SearchUIModel.LoadingUIModel

            return isSameEmpty || isSameFirstLook || isSameError || isSameWeather || isSameLoading
        }

        override fun areContentsTheSame(oldItem: SearchUIModel, newItem: SearchUIModel) =
            oldItem == newItem

    }
}
