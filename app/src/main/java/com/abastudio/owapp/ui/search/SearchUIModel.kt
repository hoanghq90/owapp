package com.abastudio.owapp.ui.search

import com.abastudio.owapp.model.Weather

sealed class SearchUIModel {
    object Empty : SearchUIModel()
    object FirstLookUIModel : SearchUIModel()
    object LoadingUIModel : SearchUIModel()
    class ErrorUIMode(val message: String) : SearchUIModel() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as ErrorUIMode

            if (message != other.message) return false

            return true
        }

        override fun hashCode(): Int {
            return message.hashCode()
        }
    }

    class WeatherUIMode(val weatherInDays: Weather) : SearchUIModel() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            if (!super.equals(other)) return false

            other as WeatherUIMode

            if (weatherInDays != other.weatherInDays) return false

            return true
        }

        override fun hashCode(): Int {
            var result = super.hashCode()
            result = 31 * result + weatherInDays.hashCode()
            return result
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }


}