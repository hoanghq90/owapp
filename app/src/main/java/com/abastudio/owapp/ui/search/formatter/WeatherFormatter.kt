package com.abastudio.owapp.ui.search.formatter

import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class WeatherFormatter : IWeatherFormatter {
    override fun formatDatetime(dateTime: Date?): String {
        return dateTime?.let {
            SimpleDateFormat("EEE, dd MMM yyy", Locale.getDefault()).format(
                dateTime
            )
        } ?: ""
    }

    override fun formatHumidity(humidity: Int?): String {
        return humidity?.let { "$humidity%" } ?: ""
    }

    override fun formatPressure(pressure: Int?): String {
        return pressure?.let { "$pressure" } ?: ""
    }

    override fun formatTemperature(averageTemperature: Double?): String {
        return averageTemperature?.let { "${it.roundToInt()}°C" } ?: ""
    }
}