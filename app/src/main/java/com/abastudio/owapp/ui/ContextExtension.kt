package com.abastudio.owapp.ui

import android.content.Context
import com.abastudio.owapp.configuration.IConfiguration

fun Context.updateConfiguration(config: IConfiguration) {
    val configuration = resources.configuration
    configuration?.fontScale = config.getTextScale()
    val metrics = resources.displayMetrics
    metrics.scaledDensity = configuration.fontScale * metrics.density
    createConfigurationContext(configuration)
}