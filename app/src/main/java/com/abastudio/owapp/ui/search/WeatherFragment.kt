package com.abastudio.owapp.ui.search

import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.abastudio.owapp.R
import com.abastudio.owapp.databinding.WeatherFragmentBinding
import com.abastudio.owapp.ui.BaseFragment
import com.abastudio.owapp.ui.search.adapter.WeatherAdapter
import com.abastudio.owapp.viewmodel.WeatherViewModel


class WeatherFragment : BaseFragment() {
    private lateinit var viewModel: WeatherViewModel
    private lateinit var viewBinding: WeatherFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(WeatherViewModel::class.java)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        state: Bundle?
    ): View {
        viewBinding = WeatherFragmentBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.setting) {
            findNavController().navigate(R.id.action_weatherFragment_to_settingFragment)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, state: Bundle?) {
        super.onViewCreated(view, state)

        viewBinding.rcvResult.apply {
            adapter = WeatherAdapter()
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }
        viewModel.getSearchWeatherLiveData().observe(viewLifecycleOwner, { searchData ->
            (viewBinding.rcvResult.adapter as WeatherAdapter).submitList(searchData)
        })
        viewBinding.btnSearch.setOnClickListener {
            viewModel.fetchForecast(viewBinding.edtSearch.text.toString())

        }
        viewBinding.edtSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.fetchForecast(viewBinding.edtSearch.text.toString())
                true
            } else false
        }
    }


}