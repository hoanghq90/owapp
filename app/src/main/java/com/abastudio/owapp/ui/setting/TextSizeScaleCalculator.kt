package com.abastudio.owapp.ui.setting

import java.math.RoundingMode
import kotlin.math.roundToInt

class TextSizeScaleCalculator {
    fun processToScale(process: Int, max: Int, maxScale: Float): Float {
        return (1 + (process * (maxScale - 1)) / max).toBigDecimal().setScale(2, RoundingMode.UP).toFloat()
    }

    fun scaleToProcess(scale: Float, max: Int, maxScale: Float): Int {
        return (((scale - 1) * (max)) / (maxScale-1)).roundToInt()
    }

}