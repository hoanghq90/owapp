package com.abastudio.owapp.ui.search.holder

import android.view.View
import com.abastudio.owapp.databinding.ItemWeatherErrorBinding
import com.abastudio.owapp.ui.search.SearchUIModel

class ErrorHolder(itemView: View) : ItemHolder(itemView) {
    private val viewBinding: ItemWeatherErrorBinding = ItemWeatherErrorBinding.bind(itemView)
    fun bind(errorUIMode: SearchUIModel.ErrorUIMode) {
        viewBinding.message.text = errorUIMode.message
    }
}