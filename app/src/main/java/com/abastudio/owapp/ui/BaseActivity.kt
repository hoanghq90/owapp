package com.abastudio.owapp.ui

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import org.koin.android.ext.android.get

open class BaseActivity : AppCompatActivity() {
    override fun attachBaseContext(newBase: Context?) {
        newBase?.updateConfiguration(get())
        super.attachBaseContext(newBase)
    }
}