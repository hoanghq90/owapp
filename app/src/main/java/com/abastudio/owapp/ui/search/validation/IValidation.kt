package com.abastudio.owapp.ui.search.validation

interface IValidation<T> {
    fun isValid(validatingValue: T): Boolean
}