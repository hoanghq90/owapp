package com.abastudio.owapp.ui.search.holder

import android.view.View
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView

abstract class ItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    fun getString(@StringRes id: Int) = itemView.context.getString(id)
}
