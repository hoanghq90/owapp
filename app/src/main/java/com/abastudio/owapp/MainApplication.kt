package com.abastudio.owapp

import android.app.Application
import com.abastudio.owapp.di.DependencyInjectionProvider

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initDI()
    }

    private fun initDI() {
        DependencyInjectionProvider().provide().start(applicationContext)
    }
}