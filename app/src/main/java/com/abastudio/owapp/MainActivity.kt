package com.abastudio.owapp

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.abastudio.owapp.databinding.MainActivityBinding
import com.abastudio.owapp.ui.BaseActivity
import com.scottyab.rootbeer.RootBeer

class MainActivity : BaseActivity() {

    private lateinit var viewBinding: MainActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = MainActivityBinding.inflate(layoutInflater)
        val view = viewBinding.root
        setContentView(view)
        val navController = (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment).navController
        setSupportActionBar(viewBinding.toolbar)
        viewBinding.toolbar.setupWithNavController(navController, AppBarConfiguration(navGraph = navController.graph))
        viewBinding.rootWarning.visibility = if (RootBeer(this).isRooted) View.VISIBLE else View.GONE
    }
}