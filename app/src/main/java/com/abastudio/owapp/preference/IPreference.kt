package com.abastudio.owapp.preference

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

interface IPreference {
    fun <S : Any, P: Any> encrypt(
        key: (KProperty<*>) -> String = KProperty<*>::name,
        default: P
    ): ReadWriteProperty<S, P>
}