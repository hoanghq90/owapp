package com.abastudio.owapp.preference

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class EncryptPreference(applicationContext: Context) : IPreference {
    private val preference: SharedPreferences

    init {
        val mainKey = MasterKey.Builder(applicationContext)
            .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
            .build()
        val sharedPrefsFile = "personal_information"
        preference = EncryptedSharedPreferences.create(
            applicationContext,
            sharedPrefsFile,
            mainKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }

    override fun <S:Any, T : Any> encrypt(key: (KProperty<*>) -> String, default: T): ReadWriteProperty<S, T> =
        Encrypt(key, default)

    inner class Encrypt<S: Any, T : Any>(var key: (KProperty<*>) -> String = KProperty<*>::name, var default: T) : ReadWriteProperty<S, T> {
        override fun setValue(thisRef: S, property: KProperty<*>, value: T) {
            preference.edit().apply {
                when (value) {
                    is Int -> putInt(key(property), value)
                    is Long -> putLong(key(property), value)
                    is String -> putString(key(property), value)
                    is Float -> putFloat(key(property), value)
                    else -> throw IllegalStateException("Not support ${value::class.java} yet")
                }
            }.apply()
        }

        @Suppress("UNCHECKED_CAST")
        override fun getValue(thisRef: S, property: KProperty<*>): T {
            return preference.run {
                when (default) {
                    is Int -> getInt(key(property), default as Int)
                    is Long -> getLong(key(property), default as Long)
                    is String -> getString(key(property), default as String)
                    is Float -> getFloat(key(property), default as Float)
                    else -> throw IllegalStateException("Not support ${default.javaClass} yet")
                }
            } as T
        }
    }


}