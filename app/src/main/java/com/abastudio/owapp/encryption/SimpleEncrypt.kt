package com.abastudio.owapp.encryption

import android.util.Base64
import java.nio.charset.StandardCharsets


class SimpleEncrypt : IEncrypt {
    override fun decode(key: String): String =
        String(Base64.decode(key, Base64.DEFAULT), StandardCharsets.UTF_8)

    override fun encode(value: String): String =
        Base64.encodeToString(value.toByteArray(StandardCharsets.UTF_8), Base64.DEFAULT)
}