package com.abastudio.owapp.encryption

interface IEncrypt {
    fun decode(key: String): String
    fun encode(value: String): String
}