package com.abastudio.owapp.di

import android.content.Context

interface IDependenceInjection{
    fun start(applicationContext: Context)
}