package com.abastudio.owapp.di

import com.abastudio.owapp.di.koin.DependenceInjection

class DependencyInjectionProvider {
    fun provide(): IDependenceInjection = DependenceInjection()//Using Koin to DI
}