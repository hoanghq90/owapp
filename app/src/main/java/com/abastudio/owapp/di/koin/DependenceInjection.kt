package com.abastudio.owapp.di.koin

import android.content.Context
import com.abastudio.owapp.di.IDependenceInjection
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class DependenceInjection : IDependenceInjection {
    override fun start(applicationContext: Context) {
        startKoin {
            androidContext(applicationContext)
            modules(
                applicationModule,
            )
        }
    }

}