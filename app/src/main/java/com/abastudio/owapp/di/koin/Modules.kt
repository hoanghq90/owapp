package com.abastudio.owapp.di.koin

import com.abastudio.owapp.configuration.ConfigurationImpl
import com.abastudio.owapp.configuration.IConfiguration
import com.abastudio.owapp.data.IWeatherRepository
import com.abastudio.owapp.data.WeatherRepository
import com.abastudio.owapp.networking.IService
import com.abastudio.owapp.networking.NetworkingProvider
import com.abastudio.owapp.preference.EncryptPreference
import com.abastudio.owapp.preference.IPreference
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val applicationModule = module {
    single<IPreference> { EncryptPreference(applicationContext = androidApplication()) }
    single<IService> {
        NetworkingProvider().build(androidApplication())
    }
    single<IWeatherRepository> { WeatherRepository() }
    single<IConfiguration> { ConfigurationImpl(get()) }
}

