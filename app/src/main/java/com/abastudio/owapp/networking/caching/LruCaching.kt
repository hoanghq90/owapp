package com.abastudio.owapp.networking.caching

import android.util.LruCache
import com.abastudio.owapp.model.Weather
import com.abastudio.owapp.ui.search.SearchQueryParams


class LruCaching : ICaching {
    var cacheSize = 4 * 1024 * 1024 // 4MiB

    val cache: LruCache<SearchQueryParams, List<Weather>> = LruCache(cacheSize)
    override fun get(params: SearchQueryParams): List<Weather>? {
        synchronized(cache) {
            return cache.get(params)
        }
    }

    override fun push(params: SearchQueryParams, weathers: List<Weather>): List<Weather>? {
        synchronized(cache) {
            if (cache.get(params) == null) {
                cache.put(params, weathers);
            }
            return weathers
        }
    }
}