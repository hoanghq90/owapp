package com.abastudio.owapp.networking

import android.content.Context

interface INetworking {
    fun generate(context: Context): IService
}