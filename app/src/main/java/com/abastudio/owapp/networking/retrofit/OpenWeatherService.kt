package com.abastudio.owapp.networking.retrofit

import com.abastudio.owapp.networking.retrofit.model.ForecastData
import retrofit2.http.GET
import retrofit2.http.Query


interface OpenWeatherService {
    @GET("forecast/daily")
    suspend fun getForecast(@Query("q") query: String, @Query("cnt") cnt: Int, @Query("units") unit: String): ForecastData
}