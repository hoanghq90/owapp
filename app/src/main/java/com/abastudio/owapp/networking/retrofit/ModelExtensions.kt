package com.abastudio.owapp.networking.retrofit

import com.abastudio.owapp.model.Weather
import com.abastudio.owapp.networking.retrofit.model.Forecast
import java.util.*

fun Forecast.toModel(): Weather {
    return Weather().also { weather ->
        weather.dateTime = Date(this@toModel.datetime * 1000L)
        weather.averageTemperature = this.temperature.min.plus(this.temperature.max).div(2)
        weather.pressure = this.pressure
        weather.humidity = this.humidity
        weather.description = this.weather.firstOrNull()?.description
    }
}