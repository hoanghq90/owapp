package com.abastudio.owapp.networking.retrofit.model

import com.abastudio.owapp.model.Weather
import com.abastudio.owapp.networking.IService
import com.abastudio.owapp.networking.retrofit.OpenWeatherService
import com.abastudio.owapp.networking.retrofit.toModel

class OpenWeatherServiceImpl(private val retrofitOpenWeather: OpenWeatherService) : IService {
    override suspend fun getWeather(query: String, numberOfForecastDays: Int, unit: String): List<Weather> {
        return retrofitOpenWeather.getForecast(
            query,
            numberOfForecastDays,
            unit
        ).list.map { it.toModel() }
    }

}