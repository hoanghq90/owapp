package com.abastudio.owapp.networking.retrofit.model

import com.google.gson.annotations.SerializedName

data class Forecast(

    @SerializedName("dt")
	val datetime : Int,
    @SerializedName("sunrise")
	val sunrise : Int,
    @SerializedName("sunset")
	val sunset : Int,
    @SerializedName("temp")
	val temperature : Temperature,
    @SerializedName("feels_like")
	val feelsLike : FeelsLike,
    @SerializedName("pressure")
	val pressure : Int,
    @SerializedName("humidity")
	val humidity : Int,
    @SerializedName("weather")
	val weather : List<Weather>,
    @SerializedName("speed")
	val speed : Double,
    @SerializedName("deg")
	val degrees : Int,
    @SerializedName("clouds")
	val clouds : Int,
    @SerializedName("pop")
	val probability : Double,
    @SerializedName("rain")
	val rain : Double
)