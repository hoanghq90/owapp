package com.abastudio.owapp.networking.caching

import com.abastudio.owapp.model.Weather
import com.abastudio.owapp.ui.search.SearchQueryParams

class SimpleCaching : ICaching {
    private val hashMap: LinkedHashMap<SearchQueryParams, List<Weather>> = linkedMapOf()
    override fun get(params: SearchQueryParams): List<Weather>? {
        return hashMap[params]
    }

    override fun push(params: SearchQueryParams, weathers: List<Weather>): List<Weather>? {
        return hashMap.put(params, weathers)
    }
}