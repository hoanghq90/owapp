package com.abastudio.owapp.networking

import com.abastudio.owapp.model.Weather

interface IService {
    suspend fun getWeather(query: String, numberOfForecastDays: Int, unit: String): List<Weather>
}