package com.abastudio.owapp.networking

import android.content.Context
import com.abastudio.owapp.networking.retrofit.RetrofitNetworking

class NetworkingProvider {
    fun build(androidApplication: Context): IService =
        RetrofitNetworking().run { generate(androidApplication) }
}