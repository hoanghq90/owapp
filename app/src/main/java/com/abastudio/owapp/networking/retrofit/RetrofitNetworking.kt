package com.abastudio.owapp.networking.retrofit

import android.content.Context
import com.abastudio.owapp.BuildConfig
import com.abastudio.owapp.encryption.EncryptProvider
import com.abastudio.owapp.networking.INetworking
import com.abastudio.owapp.networking.IService
import com.abastudio.owapp.networking.WrapperRequestInterceptor
import com.abastudio.owapp.networking.retrofit.model.OpenWeatherServiceImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RetrofitNetworking : INetworking {
    override fun generate(context: Context): IService {
        val interceptorLog = HttpLoggingInterceptor()
        interceptorLog.setLevel(HttpLoggingInterceptor.Level.BODY)

        val build = EncryptProvider().provide()
        val apiKey = build.decode(BuildConfig.API_KEY)
        val apiValue = build.decode(BuildConfig.API_VALUE)
        val baseURL = build.decode(BuildConfig.BASE_URL)

        val interceptor = WrapperRequestInterceptor(apiKey, apiValue)
        val okHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(interceptor)
            .addNetworkInterceptor(interceptorLog)
            .build()

        val retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseURL)//Push to gradle
            .build()

        return OpenWeatherServiceImpl(retrofit.create(OpenWeatherService::class.java))
    }
}