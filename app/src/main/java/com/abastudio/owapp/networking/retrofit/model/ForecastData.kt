package com.abastudio.owapp.networking.retrofit.model

import com.google.gson.annotations.SerializedName

data class ForecastData (
    @SerializedName("city")
	val city : City,
    @SerializedName("cod")
	val cod : Int,
    @SerializedName("message")
	val message : Double,
    @SerializedName("cnt")
	val cnt : Int,
    @SerializedName("list")
	val list : List<Forecast>
)