package com.abastudio.owapp.networking.caching

import com.abastudio.owapp.model.Weather
import com.abastudio.owapp.ui.search.SearchQueryParams

interface ICaching {
    fun get(params: SearchQueryParams): List<Weather>?
    fun push(params: SearchQueryParams, weathers: List<Weather>): List<Weather>?

}