package com.abastudio.owapp.configuration

data class ConfigurationState (var scale: Float)