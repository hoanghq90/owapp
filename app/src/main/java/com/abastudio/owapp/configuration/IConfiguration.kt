package com.abastudio.owapp.configuration

import com.abastudio.owapp.model.TemperatureUnit

interface IConfiguration {
    fun setUnit(unit: TemperatureUnit)
    fun getUnit(): TemperatureUnit
    fun setTextScale(textScale: Float)
    fun getTextScale(): Float
    fun saveState()
    fun hasChange(): Boolean
    fun setHistory(configurationHistory: ConfigurationHistory)
    fun restore()
}