package com.abastudio.owapp.configuration

import com.abastudio.owapp.model.TemperatureUnit
import com.abastudio.owapp.preference.IPreference

open class ConfigurationImpl(encryptPreference: IPreference) : IConfiguration {
    private var configurationHistory: ConfigurationHistory = ConfigurationHistory()
    private var _unit: String by encryptPreference.encrypt(
        key = { "unit" },
        TemperatureUnit.METRIC.value
    )
    protected open var _textScale: Float by encryptPreference.encrypt(
        key = { "text_scale" },
        1F
    )

    override fun setUnit(unit: TemperatureUnit) {
        _unit = unit.value
    }

    override fun getUnit(): TemperatureUnit = when (_unit) {
        TemperatureUnit.METRIC.value -> TemperatureUnit.METRIC
        TemperatureUnit.IMPERIAL.value -> TemperatureUnit.IMPERIAL
        else -> TemperatureUnit.STANDARD
    }

    override fun setTextScale(textScale: Float) {
        _textScale = textScale
    }

    override fun getTextScale(): Float {
        return _textScale
    }

    override fun saveState() {
        this.configurationHistory.push(ConfigurationState(_textScale))
    }

    override fun hasChange(): Boolean = _textScale != configurationHistory.pop()?.scale

    override fun setHistory(configurationHistory: ConfigurationHistory) {
        this.configurationHistory = configurationHistory
    }

    override fun restore() {
        configurationHistory.pop()?.let {
            this._textScale = it.scale
        }

    }
}