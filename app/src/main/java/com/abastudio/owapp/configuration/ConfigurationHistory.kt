package com.abastudio.owapp.configuration

import java.util.*

class ConfigurationHistory {

    private val editorState: List<ConfigurationState> = LinkedList()

    fun push(state: ConfigurationState) {
        (editorState as LinkedList).push(state)
    }

    fun pop(): ConfigurationState? = (editorState as LinkedList).run {
        if (isEmpty()) null
        else
            pop()

    }
}