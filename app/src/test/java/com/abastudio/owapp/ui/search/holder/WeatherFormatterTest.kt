package com.abastudio.owapp.ui.search.holder

import com.abastudio.owapp.ui.search.formatter.IWeatherFormatter
import com.abastudio.owapp.ui.search.formatter.WeatherFormatter
import junit.framework.TestCase
import java.util.*

class WeatherFormatterTest : TestCase() {
    private lateinit var formatter: IWeatherFormatter
    public override fun setUp() {
        super.setUp()
        formatter = WeatherFormatter()
    }

    fun testFormatDatetime() {
        val date = Date(1605759391125L)
        assertEquals("", formatter.formatDatetime(null))
        assertEquals("Thu, 19 Nov 2020", formatter.formatDatetime(date))
    }

    fun testFormatHumidity() {
        assertEquals("", formatter.formatHumidity(null))
        assertEquals("71%", formatter.formatHumidity(71))
    }

    fun testFormatPressure() {
        assertEquals("", formatter.formatPressure(null))
        assertEquals("1201", formatter.formatPressure(1201))
    }

    fun testFormatTemperature() {
        assertEquals("", formatter.formatTemperature(null))
        assertEquals("12°C", formatter.formatTemperature(12.0))
    }
}