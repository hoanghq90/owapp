package com.abastudio.owapp.ui.setting

import junit.framework.TestCase

class TextSizeScaleCalculatorTest : TestCase() {
    private lateinit var calculatorTest: TextSizeScaleCalculator
    private val MAX = 4
    private val MAX_SCALE = 1.8F

    override fun setUp() {
        super.setUp()
        calculatorTest = TextSizeScaleCalculator()
    }
    fun testProcessToScale() {
        assertEquals(1f, calculatorTest.processToScale(process = 0, max = MAX, maxScale = MAX_SCALE))
        assertEquals(1.2f, calculatorTest.processToScale(process = 1, max = MAX, maxScale = MAX_SCALE))
        assertEquals(1.4f, calculatorTest.processToScale(process = 2, max = MAX, maxScale = MAX_SCALE))
        assertEquals(1.6f, calculatorTest.processToScale(process = 3, max = MAX, maxScale = MAX_SCALE))
        assertEquals(MAX_SCALE, calculatorTest.processToScale(process = 4, max = MAX, maxScale = MAX_SCALE))
    }

    fun testScaleToProcess() {
        assertEquals(0, calculatorTest.scaleToProcess(scale = 1F, max = MAX, maxScale = MAX_SCALE))
        assertEquals(1, calculatorTest.scaleToProcess(scale = 1.2F, max = MAX, maxScale = MAX_SCALE))
        assertEquals(2, calculatorTest.scaleToProcess(scale = 1.4F, max = MAX, maxScale = MAX_SCALE))
        assertEquals(3, calculatorTest.scaleToProcess(scale = 1.6F, max = MAX, maxScale = MAX_SCALE))
        assertEquals(4, calculatorTest.scaleToProcess(scale = MAX_SCALE, max = MAX, maxScale = MAX_SCALE))
    }
}