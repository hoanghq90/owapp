package com.abastudio.owapp.ui.search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.abastudio.owapp.configuration.ConfigurationImpl
import com.abastudio.owapp.configuration.IConfiguration
import com.abastudio.owapp.data.IWeatherRepository
import com.abastudio.owapp.model.TemperatureUnit
import com.abastudio.owapp.model.Weather
import com.abastudio.owapp.viewmodel.WeatherViewModel
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class WeatherViewModelTest : TestCase(), KoinTest {
    private lateinit var viewModel: WeatherViewModel
    private lateinit var repositoryMock: IWeatherRepository
    private lateinit var configurationMock: IConfiguration

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(
            module {
                single<IWeatherRepository> { Mockito.mock(IWeatherRepository::class.java) }
                single<IConfiguration> { Mockito.mock(ConfigurationImpl::class.java) }
            })
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        Mockito.mock(clazz.java)
    }

    @Before
    public override fun setUp() {
        super.setUp()
        repositoryMock = declareMock()
        configurationMock = declareMock()
        viewModel = WeatherViewModel()
        Mockito.`when`(configurationMock.getUnit()).thenReturn(TemperatureUnit.METRIC)
    }

    @After
    public override fun tearDown() {
    }

    @Test
    fun testFetchForecast_firstLook() = runBlocking {
        val expect: List<SearchUIModel> = listOf(SearchUIModel.FirstLookUIModel)
        assertEquals(expect, getValue())
    }

    @Test
    fun testFetchForecast_errorValidation() = runBlocking {
        val expect: List<SearchUIModel> =
            listOf(SearchUIModel.ErrorUIMode("The query must least 3 character"))
        viewModel.processForecastRequest(" ")
        assertEquals(expect, getValue())

    }

    @Test
    fun testFetchForecast_error() = runBlocking {
        val expect: List<SearchUIModel> = listOf(SearchUIModel.ErrorUIMode("An unexpected error occurred"))
        Mockito.`when`(repositoryMock.getWeather(any(SearchQueryParams::class.java)))
            .thenThrow(Exception("mock"))
        viewModel.processForecastRequest("mock")
        assertEquals(expect, getValue())
    }

    @Test
    fun testFetchForecast_empty() = runBlocking {
        val expect: List<SearchUIModel> = listOf(SearchUIModel.Empty)
        Mockito.`when`(repositoryMock.getWeather(any(SearchQueryParams::class.java)))
            .thenThrow(HttpException::class.java)
        viewModel.processForecastRequest("mock")
        assertEquals(expect, getValue())
    }

    @Test
    fun testFetchForecast_data() = runBlocking {
        val response: List<Weather> = listOf(
            Weather(Date(1605759391125L), 1.0, 1, 1, "1"),
            Weather(Date(1605759391125L), 2.0, 2, 2, "2"),
            Weather(Date(1605759391125L), 3.0, 3, 3, "3"),

            )
        val expect: List<SearchUIModel> = listOf(
            SearchUIModel.WeatherUIMode(response[0]),
            SearchUIModel.WeatherUIMode(response[1]),
            SearchUIModel.WeatherUIMode(response[2])
        )
        Mockito.`when`(repositoryMock.getWeather(any(SearchQueryParams::class.java)))
            .thenReturn(response)

        viewModel.processForecastRequest("mock")

        assertEquals(expect, getValue())
    }

    fun getValue() = viewModel.getSearchWeatherLiveData().value

    @Suppress("UNCHECKED_CAST")
    private fun <T> any(type: Class<T>): T {
        Mockito.any(type)
        return null as T
    }

}