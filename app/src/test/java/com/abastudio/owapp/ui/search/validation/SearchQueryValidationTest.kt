package com.abastudio.owapp.ui.search.validation

import junit.framework.TestCase

class SearchQueryValidationTest : TestCase() {

    private lateinit var validation: SearchQueryValidation
    public override fun setUp() {
        super.setUp()
        validation = SearchQueryValidation()
    }

    fun testIsValid() {
        //Testing valid false
        assertEquals(false, validation.isValid(""))
        assertEquals(false, validation.isValid("       "))
        assertEquals(false, validation.isValid("a"))
        assertEquals(false, validation.isValid("a "))
        assertEquals(false, validation.isValid("aa"))
        assertEquals(false, validation.isValid("aa "))
        //Testing valid false
        assertEquals(true, validation.isValid("aaa"))
        assertEquals(true, validation.isValid("aaa "))
    }
}