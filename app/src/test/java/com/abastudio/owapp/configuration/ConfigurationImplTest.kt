package com.abastudio.owapp.configuration

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.abastudio.owapp.preference.EncryptPreference
import com.abastudio.owapp.preference.IPreference
import junit.framework.TestCase
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ConfigurationImplTest : TestCase(), KoinTest {

    private lateinit var configurationImpl: ConfigurationImpl
    private lateinit var prefer: IPreference
    private lateinit var history: ConfigurationHistory
    private val SCALE_TEXT = 1990F

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(
            module {
                single<IPreference> { Mockito.mock(EncryptPreference::class.java) }
                single<IConfiguration> { Mockito.mock(ConfigurationImpl::class.java) }
            })
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        Mockito.mock(clazz.java)
    }

    @Before
    public override fun setUp() {
        super.setUp()
        prefer = declareMock()
        configurationImpl = object : ConfigurationImpl(prefer){
            override var _textScale = SCALE_TEXT
        }
        history = Mockito.mock(ConfigurationHistory::class.java)
        configurationImpl.setHistory(history)
    }

    @Test
    fun testSaveState() {
        configurationImpl.saveState()
        Mockito.verify(history).push(any(ConfigurationState::class.java))
    }

    @Test
    fun testHasChange_true() {
        Mockito.`when`(history.pop()).thenReturn(ConfigurationState(SCALE_TEXT))
        configurationImpl.saveState()
        configurationImpl.setTextScale(2F)
        val hasChange = configurationImpl.hasChange()
        Mockito.verify(history).pop()
        assertEquals(true, hasChange)
    }
    @Test
    fun testHasChange_fail() {
        Mockito.`when`(history.pop()).thenReturn(ConfigurationState(SCALE_TEXT))
        configurationImpl.saveState()
        configurationImpl.setTextScale(2F)
        val hasChange = configurationImpl.hasChange()
        Mockito.verify(history).pop()
        assertEquals(true, hasChange)
    }
    @Test
    fun testHasChange_restore() {
        Mockito.`when`(history.pop()).thenReturn(ConfigurationState(SCALE_TEXT))
        configurationImpl.saveState()
        configurationImpl.setTextScale(2F)
        configurationImpl.restore()
        Mockito.verify(history).pop()
        assertEquals(SCALE_TEXT, configurationImpl.getTextScale())
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> any(type: Class<T>): T {
        Mockito.any(type)
        return null as T
    }

}
